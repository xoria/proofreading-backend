import uuid from "uuid/v4";
import {createDocument} from "./memory";

/*===================================================== Exports  =====================================================*/

export const notifications = {};
export const queries = {create};

/*==================================================== Functions  ====================================================*/

function create() {
  const id = uuid();
  createDocument(id);
  return {documentId: id};
}

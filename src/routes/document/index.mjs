import {notifications as nCreate, queries as qCreate} from "./create";
import {notifications as nAuthority, queries as qAuthority} from "./authority";
import {notifications as nFetch, queries as qFetch} from "./fetch";

/*===================================================== Exports  =====================================================*/

export default {
  prefix: "document+",
  queries: {
    ...qCreate,
    ...qFetch,
    ...qAuthority,
  },
  notifications: {
    ...nCreate,
    ...nFetch,
    ...nAuthority,
  }
};

/*==================================================== Functions  ====================================================*/



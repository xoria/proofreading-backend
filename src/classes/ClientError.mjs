export default class ClientError extends Error {

  constructor(status, message, payload) {
    super(message);
    this.status = status;
    this.payload = payload;
  }

  toMessage() {
    if (this.payload == null) { return this.message; }
    return {message: this.message, payload: this.payload};
  }

}

import WebSocket from "ws";
import config from "../config";
import Session from "../../classes/Session";
import {handleNotification, handleQuery} from "./bus";
import {parseMessage, validateIncomingMessage} from "./message-parse";

const PING_INTERVAL = 15000;

let nextMessageId = 0;

/*===================================================== Exports  =====================================================*/

export {start};

/*==================================================== Functions  ====================================================*/

function start() {
  const wss = new WebSocket.Server({port: config.port});

  wss.on("connection", (socket) => {
    socket.isAlive = true;
    socket.on("pong", heartbeat);

    const session = new Session(socket);
    session.logger.debug("new session connected");

    session.socket.on("message", (messageString) => {
      try {
        validateIncomingMessage(messageString);
      } catch (err) {
        session.logger.debug("received message failed validation", {err});
        session.notifyError(err.message);
        return;
      }
      const messageId = nextMessageId++;
      const logger = session.logger.child({_id: messageId, message: messageString});
      logger.debug("received message");
      let message;
      try {
        message = parseMessage(messageString, messageId);
      } catch (err) {
        logger.debug("message failed parsing", {err});
        session.notifyError(err.message);
        return;
      }
      const scope = {message, payload: message.payload, logger, session};
      (message.isQuery ? handleQuery : handleNotification)(scope);
    });

  });

  setInterval(function ping() {
    for (let ws of wss.clients) {
      if (ws.isAlive === false) {
        ws.terminate();
      } else {
        ws.isAlive = false;
        ws.ping(noop);
      }
    }
  }, PING_INTERVAL);

  return wss;
}

function noop() {}

// eslint-disable-next-line no-invalid-this
function heartbeat() { this.isAlive = true; }

import ClientError from "../../classes/ClientError";

const NOTIFY_HANDLERS = {};
const QUERY_HANDLERS = {};

/*===================================================== Exports  =====================================================*/

export {
  onQuery, handleQuery,
  onNotification, handleNotification,
};

/*==================================================== Functions  ====================================================*/

function onNotification(type, fn) {
  if (NOTIFY_HANDLERS.hasOwnProperty(type)) {
    throw new Error("Notification handler type already exists.");
  }
  NOTIFY_HANDLERS[type] = fn;
}

function onQuery(type, fn) {
  if (QUERY_HANDLERS.hasOwnProperty(type)) { throw new Error("Query handler type already exists."); }
  QUERY_HANDLERS[type] = fn;
}

async function handleNotification(scope) {
  const message = scope.message;
  if (!NOTIFY_HANDLERS.hasOwnProperty(message.type)) {
    scope.logger.error("notification aborted");
    scope.session.notifyError("Unknown notification type: " + message.type);
    return;
  }
  try {
    const promise = NOTIFY_HANDLERS[message.type](scope);
    if (promise != null && typeof promise.then === "function") { await promise; }
  } catch (err) {
    if (err instanceof ClientError) {
      return scope.session.notifyError({ reason: err.toMessage(), status: err.status });
    }
    scope.session.notifyError("Internal server error.");
    scope.logger.err(err, "notification finished with error");
    return;
  }
  scope.logger.debug("notification finished successfully");
}

async function handleQuery(scope) {
  const session = scope.session;
  const message = scope.message, queryId = message.queryId;
  if (!QUERY_HANDLERS.hasOwnProperty(message.type)) {
    scope.logger.error("query aborted");
    session.respondError(queryId, "Unknown query type.", 400);
    return;
  }
  let result;
  try {
    const promise = QUERY_HANDLERS[message.type](scope);
    result = promise != null && typeof promise.then === "function" ? await promise : promise;
  } catch (err) {
    if (err instanceof ClientError) {
      return session.respondError(queryId, err.toMessage(), err.status);
    }
    scope.logger.err(err, "query finished with error");
    session.respondError(queryId, "Internal server error.", 500);
    return;
  }
  scope.logger.debug("query finished successfully");
  session.respondSuccess(queryId, result);
}
